import java.io.FilePermission;
import java.security.AccessController;

public class AccessController2 {
    public static void main(String[] args) {
        FilePermission perm = new FilePermission("/temp/testFile", "read");
        AccessController.checkPermission(perm);
    }
}
