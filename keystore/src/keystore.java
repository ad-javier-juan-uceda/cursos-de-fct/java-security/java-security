import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class keystore {
    public static void main(String[] args) {
        //Creating the KeyStore object
        KeyStore keyStore = null;
        try {
            keyStore = KeyStore.getInstance("JCEKS");


            //Loading the KeyStore object
            char[] password = "changeit".toCharArray();
            String path = "security";
            java.io.FileInputStream fis = new FileInputStream(path);
            keyStore.load(fis, password);

            //Creating the KeyStore.ProtectionParameter object
            KeyStore.ProtectionParameter protectionParam = new KeyStore.PasswordProtection(password);

            //Creating SecretKey object
            SecretKey mySecretKey = new SecretKeySpec("myPassword".getBytes(), "DSA");

            //Creating SecretKeyEntry object
            KeyStore.SecretKeyEntry secretKeyEntry = new KeyStore.SecretKeyEntry(mySecretKey);
            keyStore.setEntry("secretKeyAlias", secretKeyEntry, protectionParam);

            //Storing the KeyStore object
            java.io.FileOutputStream fos = null;
            fos = new java.io.FileOutputStream("newKeyStoreName");
            keyStore.store(fos, password);
            System.out.println("data stored");


        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
