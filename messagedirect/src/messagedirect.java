import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Scanner;

public class messagedirect {
    public static void main(String[] args) {
        try {
            //Reading data from user
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter the message");
            String message = sc.nextLine();

            //Creating the MessageDigest object
            MessageDigest md = null;

            md = MessageDigest.getInstance("SHA-256");


            //Passing data to the created MessageDigest Object
            md.update(message.getBytes());

            //Compute the message digest
            byte[] digest = md.digest();
            System.out.println(Arrays.toString(digest));

            //Converting the byte array in to HexString format
            StringBuilder hexString = new StringBuilder();

            for (byte b : digest) {
                hexString.append(Integer.toHexString(0xFF & b));
            }
            System.out.println("Hex format : " + hexString.toString());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
