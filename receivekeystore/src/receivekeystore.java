import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

public class receivekeystore {
    public static void main(String[] args) {

        try {
            //Creating the KeyStore object
            KeyStore keyStore = KeyStore.getInstance("JCEKS");

            //Loading the the KeyStore object
            char[] password = "changeit".toCharArray();
            java.io.FileInputStream fis = new FileInputStream(
                    "security/security.txt");

            keyStore.load(fis, password);

            //Creating the KeyStore.ProtectionParameter object
            KeyStore.ProtectionParameter protectionParam = new KeyStore.PasswordProtection(password);

            //Creating SecretKey object
            SecretKey mySecretKey = new SecretKeySpec("myPassword".getBytes(), "DSA");

            //Creating SecretKeyEntry object
            KeyStore.SecretKeyEntry secretKeyEntry = new KeyStore.SecretKeyEntry(mySecretKey);
            keyStore.setEntry("secretKeyAlias", secretKeyEntry, protectionParam);

            //Storing the KeyStore object
            java.io.FileOutputStream fos = null;
            fos = new java.io.FileOutputStream("newKeyStoreName");
            keyStore.store(fos, password);

            //Creating the KeyStore.SecretKeyEntry object
            KeyStore.SecretKeyEntry secretKeyEnt = (KeyStore.SecretKeyEntry) keyStore.getEntry("secretKeyAlias", protectionParam);

            //Creating SecretKey object
            SecretKey mysecretKey = secretKeyEnt.getSecretKey();
            System.out.println("Algorithm used to generate key : " + mysecretKey.getAlgorithm());
            System.out.println("Format used for the key: " + mysecretKey.getFormat());
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        }
    }
}
